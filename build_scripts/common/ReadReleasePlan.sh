#!/bin/bash

# Set Version Infomation from Release Plan
a=0
echo ${RELEASE_TXT}
while read line

do a=$(($a+1));

if [ x${line:0:1} == 'x#' ]; then
    continue
fi
nospaceline=`echo -e "$line" | sed  -e 's/[\t ]//g'`
if [ x$nospaceline == "x" ]; then
    continue
fi

Name=${line%%:*};Value=${line#*:}
#echo $a:[$Name] = [$Value];
export $Name="$Value"
done < "${RELEASE_TXT}"
