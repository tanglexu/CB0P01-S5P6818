#!/bin/bash

function gettop
{
    local TOPFILE=build/core/envsetup.mk
    if [ -n "$TOP" -a -f "$TOP/$TOPFILE" ] ; then
        echo $TOP
    else
        if [ -f $TOPFILE ] ; then
            # The following circumlocution (repeated below as well) ensures
            # that we record the true directory name and not one that is
            # faked up with symlink names.
            PWD= /bin/pwd
        else
            # We redirect cd to /dev/null in case it's aliased to
            # a command that prints something as a side-effect
            # (like pushd)
            local HERE=$PWD
            T=
            while [ \( ! \( -f $TOPFILE \) \) -a \( $PWD != "/" \) ]; do
                cd .. > /dev/null
                T=`PWD= /bin/pwd`
            done
            cd $HERE > /dev/null
            if [ -f "$T/$TOPFILE" ]; then
                echo $T
            fi
        fi
    fi
}

trap error_handling ERR

error_handling()
{
        error_code=$?

        if [ "$1" != "" ]; then
                error_code=$1
        fi

        echo Error!!! \(code=$error_code\)
        exit $error_code
}

TOP=$(gettop)
if [ "$TOP" == "" ]; then
    echo "Couldn't locate the top of the tree.  Try setting TOP." >&2
    return
fi

# Read ReleasePlan.txt
. $TOP/build_scripts/common/ReadReleasePlan.sh
. $TOP/build_scripts/common/SetHWPlatform.sh

# Set Environment
cd $TOP
. build/envsetup.sh
lunch $RKParameter_Launch

rm -rf $OUT/root
rm -f $OUT/system/build.prop

# Build kernel modules
echo Build kernel modules
./device/nexell/tools/build.sh -b $RKParameter_BoardName -a $RKParameter_ARM_ARCH -t module

# Build Android
echo Build Android
make -j4 installclean
./device/nexell/tools/build.sh -b $RKParameter_BoardName -a $RKParameter_ARM_ARCH -t android

# beep sound
echo $'\07'

