#!/bin/bash

#WV=800x480
#SN=800*600
#WS=1024x600
#XN=1024*768
#WD=1280x720
#WE=1280x800
#EN=1280x1024
#WX=1366x768
#WP=1440x900
#WR=1600x900
#WM=1680x1050
#WH=1920x1080
#WU=1920x1200
#WD=2480x1152
#QD=2560x1440
#QE=2560x1600
#QX=2732x1536
#QH=3840x2160

if [ ! "$HWPlatform" ] ; then
	echo Please set proper HWPlatform in ReleasePlan.txt of each OEM.
	return
elif [ "$HWPlatform" = "s5p6818_drone_HDMI_1080P" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_HDMI_1080P
	export LCD_TXT=lcd_rgb_24_1920_1080.txt
	RKParameter_LCD_Resolution=WH
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_RGB_1920_1080_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_HDMI_1080p_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_HDMI_1080p_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_MIPI_1920_1200" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_MIPI_1920_1200
	export LCD_TXT=lcd_mipi_10_1920_1200.txt
	RKParameter_LCD_Resolution=WU
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_MIPI_1920_1200_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_MIPI_1920_1200_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_MIPI_1920_1200_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_RGB_800_480" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_RGB_800_480
	export LCD_TXT=lcd_rgb_7_800_480.txt
	RKParameter_LCD_Resolution=WV
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_RGB_800_480_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_RGB_800_480_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_RGB_800_480_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_RGB_1280_1024" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_RGB_1280_1024
	export LCD_TXT=lcd_rgb_17_1280_1024.txt
	RKParameter_LCD_Resolution=EN
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_RGB_1280_1024_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_RGB_1280_1024_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_RGB_1280_1024_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_RGB_1920_1200" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_RGB_1920_1200
	export LCD_TXT=lcd_rgb_24_1920_1200.txt
	RKParameter_LCD_Resolution=WU
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_RGB_1920_1200_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_RGB_1920_1200_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_RGB_1920_1200_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_RGB_1920_1080" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_RGB_1920_1080
	export LCD_TXT=lcd_rgb_24_1920_1080.txt
	RKParameter_LCD_Resolution=WH
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_RGB_1920_1080_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_RGB_1920_1080_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_RGB_1920_1080_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_LVDS_1280_800" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_LVDS_1280_800_10INCH
	export LCD_TXT=lcd_lvds_10_1280_800.txt
	RKParameter_LCD_Resolution=WE
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_LVDS_1280_800_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_LVDS_1280_800_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_LVDS_1280_800_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_LVDS_1024_768" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_LVDS_1024_768_15INCH
	export LCD_TXT=lcd_lvds_15_1024_768.txt
	RKParameter_LCD_Resolution=XN
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_LVDS_1024_768_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_LVDS_1024_768_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_LVDS_1024_768_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_LVDS_1024_600" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_LVDS_1024_600_7INCH
	export LCD_TXT=lcd_lvds_7_1024_600.txt
	RKParameter_LCD_Resolution=WS
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_LVDS_1024_600_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_LVDS_1024_600_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_LVDS_1024_600_defconfig
elif [ "$HWPlatform" = "s5p6818_drone_LVDS_800_600" ] ; then
	echo Setting HW Platform $HWPlatform
	export RP_LCD=RP_LCD_LVDS_800_600_10INCH
	export LCD_TXT=lcd_lvds_10_800_600.txt
	RKParameter_LCD_Resolution=SN
	RKParameter_Launch=aosp_s5p6818_drone-userdebug
	RKParameter_BoardName=s5p6818_drone
	RKParameter_ARM_ARCH=32
	RKParameter_Kernel=s5p6818_drone_android_lollipop_LVDS_800_600_defconfig
	RKParameter_Ubuntu_Kernel=s5p6818_drone_ubuntu_LVDS_800_600_defconfig
	RKParameter_QT_Kernel=s5p6818_drone_qt_LVDS_800_600_defconfig
else 
	echo I can not idenfied HW Platform: $HWPlatform
	echo Please add proper HWPlatfmr setting to TOP/build_scripts/common/SetHWPlatform.sh
fi


