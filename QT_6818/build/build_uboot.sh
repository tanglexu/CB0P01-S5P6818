#/bin/sh

echo "build uboot..."

TOP=`pwd`
result_dir=$TOP/result
top=$TOP

BOARD_NAME=s5p6818_drone
toolchain_version=4.8
cpu_num=4

echo "top : $top"


    if [ ! -d $top/build/prebuilts/gcc/linux-x86/arm/arm-eabi-$toolchain_version/bin ]; then
        echo "Error: can't find android toolchain!!!"
        echo "Check android source"
        exit 1
    fi

    #echo "PATH setting for android toolchain"
    export PATH=${top}/build/prebuilts/gcc/linux-x86/arm/arm-eabi-$toolchain_version/bin/:$PATH
    arm-eabi-gcc -v
    if [ $? -ne 0 ]; then
        echo "Error: can't check arm-eabi-gcc"
        echo "Check android source"
        exit 1
    fi



#cd $top/u-boot
#make distclean

##make clean
#make ${BOARD_NAME}_config
#make -j$cpu_num

	cp $top/u-boot/u-boot.bin $result_dir/

cd $top

echo "build uboot end"
