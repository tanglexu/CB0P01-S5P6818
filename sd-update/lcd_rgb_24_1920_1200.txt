type		=0      //RGB:0 LVDS:1 MIPI:2

out_format	=3	//CFG_DISP_PRI_OUT_FORMAT
//RGB555:0  RGB565:1  RGB666:2  RGB888:3  MRGB555A:4  MRGB555B:5  MRGB565:6  MRGB666:7  MRGB888A:8  MRGB888B:9  CCIR656:10  CCIR601A:12  CCIR601B:13


lvds_format	=1		//LVDS_LCDFORMAT_VESA:0   LVDS_LCDFORMAT_JEIDA:1
pclk		=154000000	//CFG_DISP_PRI_PIXEL_CLOCK
width		=1920		//CFG_DISP_PRI_RESOL_WIDTH
height		=1200		//CFG_DISP_PRI_RESOL_HEIGHT
hfp		=24		//CFG_DISP_PRI_HSYNC_FRONT_PORCH
hbp		=40		//CFG_DISP_PRI_HSYNC_BACK_PORCH
hls		=16		//CFG_DISP_PRI_HSYNC_SYNC_WIDTH
vfp		=3		//CFG_DISP_PRI_VSYNC_FRONT_PORCH
vbp		=26		//CFG_DISP_PRI_VSYNC_BACK_PORCH
vls		=6		//CFG_DISP_PRI_VSYNC_SYNC_WIDTH

gpio_led	=pad_gpio_c + 24 : 1 : 0	//goio num（pad_gpio_c + 24） : alt（1） :value（0）
gpio1		=pad_gpio_c + 7 : 1 : 1		//gpio 可以如下自行增加，此处修改只提供作为gpio使用，，注意使ALT选择为GPIO   如J60的4个gpio
gpio2		=pad_gpio_c + 27 : 0 : 1
gpio3		=pad_gpio_b + 30 : 1 : 1
gpio4		=pad_gpio_b + 31 : 1 : 1
