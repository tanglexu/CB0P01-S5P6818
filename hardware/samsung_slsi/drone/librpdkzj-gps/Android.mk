LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

ifeq ($(BOARD_HAVE_RPDZKJ_GPS), true)

LOCAL_CFLAGS += -DHAVE_GPS_HARDWARE

LOCAL_SRC_FILES := \
   gps_mstar.c 
 ##  gps_qemu.c
	
LOCAL_MODULE := gps.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_TAGS := debug

LOCAL_C_INCLUDES := \
	$(LOCAL_PATH) \
	hardware\libhardware_legacy

#LOCAL_LDFLAGS = $(LOCAL_PATH)/GPS_Lib_Static_Cupcake_leader.a

LOCAL_SHARED_LIBRARIES := \
	libc \
	libutils \
	libcutils \
	liblog

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_PATH := $(TARGET_OUT_SHARED_LIBRARIES)/hw

include $(BUILD_SHARED_LIBRARY)
endif
