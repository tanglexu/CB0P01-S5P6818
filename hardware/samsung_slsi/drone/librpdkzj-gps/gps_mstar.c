#include <errno.h>
#include <pthread.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <math.h>
#include <time.h>
#include <semaphore.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

#define  LOG_TAG  "gps_mstar"
#include <utils/Log.h>
//#include <cutils/log.h>
#include <cutils/sockets.h>
#include <cutils/properties.h>
#include <hardware/gps.h>


//#define GPS_DEBUG_TOKEN
//#undef GPS_DEBUG_TOKEN

//#define LOG_GPS
//#undef LOG_GPS
#ifdef LOG_GPS
#define  D(...)   ALOGD(__VA_ARGS__)
#define  GPS_DEBUG  1
#else
#define  D(...)   ((void)0)
#define  GPS_DEBUG  0
#endif

#define MAX_CMD_TRY_COUNT 32
#define MSTAR_DEVICES     "/dev/ttySAC3"
#define GPS_POWER_CTL    "/proc/rpgps"


/* Nmea Parser stuff */
#define  NMEA_MAX_SIZE  83

typedef struct {
	int valid;
	double systime;
	GpsUtcTime timestamp;
} AthTimemap_t;

typedef struct {
	int     pos;
	int     overflow;
	int     utc_year;
	int     utc_mon;
	int     utc_day;
	int     utc_diff;
	GpsLocation  fix;
	GpsSvStatus  sv_status;
	int     sv_status_changed;
	char    in[ NMEA_MAX_SIZE + 1 ];
	int     gsa_fixed;
	AthTimemap_t timemap;
} NmeaReader;


typedef struct {
	int                     init;
	int                     start;
	int                     fd;
	GpsCallbacks           callbacks;
	pthread_t               thread;
	pthread_t               task_thread;
	NmeaReader              reader;
	int                     control[2];
	int                     freq;
	int                     fixed;
} GpsState;


/* commands sent to the gps thread */
enum {
    CMD_QUIT = 0,
    CMD_START,
    CMD_STOP,
    CMD_FREQ,
    CMD_CLEAR_AID
};


GpsState  g_gps_state[1];
GpsLocation  g_fix;
GpsStatus     g_status ;
GpsSvStatus  g_svStatus ;
long long g_utc_diff = 0;


static int
open_gps_fd()
{
	int fd;
	struct termios tos;
	
	
#if 0  
	int fd1,fd2,fd3;
	char cam_file1[] = "/proc/rp_button/button_ctrl";	
	char cam_file2[] = "proc/rpgps";	
	char cam_file3[] = "dev/rp_bt_dev";
//	char cam_file4[] = "sys/class/rp_bt_dev";
	
	 ALOGD(" ");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD("                          rongpin gps");
	 ALOGD("                          www.rpdzkj.com");
	 ALOGD("                       TEL: 0755 - 23574363");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD(" ");

	fd1 = open(cam_file1,O_RDWR);	
	if(fd1 > 0 ){						
		close(fd1);	
	}	
	else{		
			
			close(fd1);	
			return -1; 
	}


	 fd2 = open(cam_file2,O_RDWR);
	if(fd2 > 0){
		close(fd2);
	}
	else{
		close(fd2);
		return -1;
	}
	
	fd3 = open(cam_file3,O_RDWR);
	if(fd3 > 0){
		close(fd3);
	}
	else{
		close(fd3);
		return -1;
	}

#endif

	fd = open(MSTAR_DEVICES,(O_RDWR | O_NOCTTY | O_NONBLOCK));

	if (fd == -1) {
		D("open_gps_fd failed: errno=%d(%s)", errno, strerror(errno));
		return -1;
	}
  tcflush(fd, TCIOFLUSH);
	tcgetattr(fd, &tos);
	tos.c_cflag &= ~PARENB;	// No par /* PARENB PARODD CSTOPB */
	tos.c_cflag &= ~CSTOPB;	// 1 stop bit /* CSTOP: 2 stop bit */
	tos.c_cflag |= CS8;
	tos.c_cflag |= CRTSCTS;
	//cfsetispeed(&tos, B115200);
	cfsetispeed(&tos, B9600);
	tcsetattr(fd, TCSANOW, &tos);
	return fd;
}

static int gps_hardware_poweroff()
{
	char buf[32];        
	int len;     
	int fd = open(GPS_POWER_CTL, O_WRONLY);
	D("gps_hardware_poweroff");

	//if (fd == -1) {
	//	D("can't open <%s> for <%s>\n", GPS_POWER_CTL, strerror(errno));
	//	return -1;
	//}

	if (fd >= 0) {        
		len = sprintf(buf,"0x30");        
		len = write(fd, buf, len);        
	
		len = sprintf(buf,"0x40");        
		len = write(fd, buf, len);   
	}
	close(fd);
	return 0;
}
static int gps_hardware_poweron()
{
	char buf[32];        
	int len;     
	int fd = open(GPS_POWER_CTL, O_WRONLY);
	D("gps_hardware_poweron");
	
#if 0  
	int fd1,fd2,fd3;
	char cam_file1[] = "/proc/rp_button/button_ctrl";	
	char cam_file2[] = "proc/rpgps";	
	char cam_file3[] = "dev/rp_bt_dev";
//	char cam_file4[] = "sys/class/rp_bt_dev";
	
	 ALOGD(" ");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD("                          rongpin gps");
	 ALOGD("                          www.rpdzkj.com");
	 ALOGD("                       TEL: 0755 - 23574363");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD(" ");

	fd1 = open(cam_file1,O_RDWR);	
	if(fd1 > 0 ){						
		close(fd1);	
	}	
	else{		
			
			close(fd1);	
			return -1; 
	}


	 fd2 = open(cam_file2,O_RDWR);
	if(fd2 > 0){
		close(fd2);
	}
	else{
		close(fd2);
		return -1;
	}
	
	fd3 = open(cam_file3,O_RDWR);
	if(fd3 > 0){
		close(fd3);
	}
	else{
		close(fd3);
		return -1;
	}

#endif

	if (fd == -1) {
		D("can't open <%s> for <%s>\n", GPS_POWER_CTL, strerror(errno));
		return -1;
	}
  if (fd >= 0) {        
		len = sprintf(buf,"0x20");        
		len = write(fd, buf, len);        
	  
	  len = sprintf(buf,"0x10");        
		len = write(fd, buf, len);   
	}
	close(fd);
	return 0;
}

static int internal_req(char command)
{
	char cmd = command;
	int ret = 0;
	int retrycount = 0;
	D("%s: send %d command: ret=%d: %s", __FUNCTION__, command, ret, strerror(errno));

	do {
		ret = write(g_gps_state->control[0], &cmd, sizeof(cmd));
		retrycount ++;
	} while ((ret < 0) && (errno == EINTR) && (retrycount < MAX_CMD_TRY_COUNT));

	if (ret != sizeof(cmd)) {
		D("%s: could not send %d command: ret=%d: %s",
		  __FUNCTION__, command, ret, strerror(errno));
		return -1;
	}

	return 0;
}

static void gps_state_done(GpsState*  s)
{
	// tell the thread to quit, and wait for it
	char   cmd = CMD_QUIT;
	void*  dummy;

	write(s->control[0], &cmd, 1);

	pthread_join(s->thread, &dummy);
	//pthread_join(s->task_thread, &dummy);
	
	// close the control socket pair
	close(s->control[0]);
	s->control[0] = -1;
	close(s->control[1]);
	s->control[1] = -1;
	//s->thread = (pthread_t)NULL;
	//s->task_thread = (pthread_t)NULL;
	
	g_status.status = GPS_STATUS_ENGINE_OFF;
	(s->callbacks).status_cb(&g_status);
	//sleep(2);
	// close connection to the QEMU GPS daemon
	//close(s->fd);
	s->fd = -1;
	//gps_hardware_poweroff();
	s->init = 0;
}

static int epoll_register(int  epoll_fd, int  fd)
{
	struct epoll_event  ev;
	int                 ret, flags;
	/* important: make the fd non-blocking */
	flags = fcntl(fd, F_GETFL);
	fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	ev.events  = EPOLLIN;
	ev.data.fd = fd;

	do {
		ret = epoll_ctl(epoll_fd, EPOLL_CTL_ADD, fd, &ev);
	} while (ret < 0 && errno == EINTR);

	return ret;
}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       N M E A   T O K E N I Z E R                     *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/

typedef struct {
	const char*  p;
	const char*  end;
} Token;

#define  MAX_NMEA_TOKENS  32

typedef struct {
	int     count;
	Token   tokens[ MAX_NMEA_TOKENS ];
} NmeaTokenizer;

static int
nmea_tokenizer_init(NmeaTokenizer*  t, const char*  p, const char*  end)
{
	int    count = 0;
	char*  q;

	// the initial '$' is optional
	if (p < end && p[0] == '$')
		p += 1;

	// remove trailing newline
	if (end > p && end[-1] == '\n') {
		end -= 1;

		if (end > p && end[-1] == '\r')
			end -= 1;
	}

	// get rid of checksum at the end of the sentecne
	if (end >= p + 3 && end[-3] == '*') {
		end -= 3;
	}

	while (p < end) {
		const char*  q = p;
		q = memchr(p, ',', end - p);

		if (q == NULL)
			q = end;

		if (count < MAX_NMEA_TOKENS) {
			t->tokens[count].p   = p;
			t->tokens[count].end = q;
			count += 1;
		}

		if (q < end)
			q += 1;

		p = q;
	}

	t->count = count;
	return count;
}

static Token
nmea_tokenizer_get(NmeaTokenizer*  t, int  index)
{
	Token  tok;
	static const char*  dummy = "";

	if (index < 0 || index >= t->count) {
		tok.p = tok.end = dummy;
	} else
		tok = t->tokens[index];

	return tok;
}


static int
str2int(const char*  p, const char*  end)
{
	int   result = 0;
	int   len    = end - p;

	if (len == 0) {
		return -1;
	}

	for (; len > 0; len--, p++) {
		int  c;

		if (p >= end)
			goto Fail;

		c = *p - '0';

		if ((unsigned)c >= 10)
			goto Fail;

		result = result * 10 + c;
	}

	return  result;
Fail:
	return -1;
}

static double
str2float(const char*  p, const char*  end)
{
	int   result = 0;
	int   len    = end - p + 1;
	char  temp[32];

	if (len == 0) {
		return -1.0;
	}

	if (len >= (int)sizeof(temp))
		return 0.;

	memcpy(temp, p, len);
	temp[len] = 0;
	return strtod(temp, NULL);
}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       N M E A   P A R S E R                           *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/

static void
nmea_reader_update_utc_diff(NmeaReader*  r)
{
	time_t         now = time(NULL);
	struct tm      tm_local;
	struct tm      tm_utc;
	long           time_local, time_utc;
	gmtime_r(&now, &tm_utc);
	localtime_r(&now, &tm_local);
	time_local = tm_local.tm_sec +
	             60 * (tm_local.tm_min +
	                   60 * (tm_local.tm_hour +
	                         24 * (tm_local.tm_yday +
	                               365 * tm_local.tm_year)));
	time_utc = tm_utc.tm_sec +
	           60 * (tm_utc.tm_min +
	                 60 * (tm_utc.tm_hour +
	                       24 * (tm_utc.tm_yday +
	                             365 * tm_utc.tm_year)));
	r->utc_diff = time_utc - time_local;
}


static void
nmea_reader_init(NmeaReader*  r)
{
	memset(r, 0, sizeof(*r));
	r->pos      = 0;
	r->overflow = 0;
	r->utc_year = -1;
	r->utc_mon  = -1;
	r->utc_day  = -1;
	nmea_reader_update_utc_diff(r);
}

static int nmea_reader_get_timestamp(NmeaReader*  r, Token  tok, time_t *timestamp)
{
	int        hour, minute;
	double     seconds;
	struct tm  tm;
	time_t     ttime;

	if (tok.p + 6 > tok.end)
		return -1;

	if (r->utc_year < 0) {
		D("no date yet, quit... ");
		return -1;
	}

	hour    = str2int(tok.p,   tok.p + 2);
	minute  = str2int(tok.p + 2, tok.p + 4);
	seconds = str2float(tok.p + 4, tok.end);
	tm.tm_hour = hour;
	tm.tm_min  = minute;
	tm.tm_sec  = (int) seconds;
	tm.tm_year = r->utc_year - 1900;
	tm.tm_mon  = r->utc_mon - 1;
	tm.tm_mday = r->utc_day;
	// D("h: %d, m: %d, s: %f", tm.tm_hour, tm.tm_min, tm.tm_sec);
	// D("Y: %d, M: %d, D: %d", tm.tm_year, tm.tm_mon, tm.tm_mday);
	nmea_reader_update_utc_diff(r);
	ttime = mktime(&tm);
	*timestamp = ttime - r->utc_diff;
	D("nmea_reader_get_timestamp: %d, %d", ttime, *timestamp);
	return 0;
}

static int
nmea_reader_update_time(NmeaReader*  r, Token  tok)
{
	time_t timestamp = 0;
	int ret = nmea_reader_get_timestamp(r, tok, &timestamp);

	if (0 == ret)
		r->fix.timestamp = (long long)timestamp * 1000;

	D("nmea_reader_update_time: %lf %lf", (double) timestamp, (double) r->fix.timestamp);
	return ret;
}

static int
nmea_reader_update_cdate(NmeaReader*  r, Token  tok_d, Token tok_m, Token tok_y)
{
	if ((tok_d.p + 2 > tok_d.end) ||
	        (tok_m.p + 2 > tok_m.end) ||
	        (tok_y.p + 4 > tok_y.end))
		return -1;

	r->utc_day = str2int(tok_d.p,   tok_d.p + 2);
	r->utc_mon = str2int(tok_m.p, tok_m.p + 2);
	r->utc_year = str2int(tok_y.p, tok_y.end + 4);
	return 0;
}

static int
nmea_reader_update_date(NmeaReader*  r, Token  date, Token  mtime)
{
	Token  tok = date;
	int    day, mon, year;

	if (tok.p + 6 != tok.end) {
		D("no date info, use host time as default: '%.*s'", tok.end - tok.p, tok.p);
		/* no date info, will use host time in _update_time function */
	}

	/* normal case */
	day  = str2int(tok.p, tok.p + 2);
	mon  = str2int(tok.p + 2, tok.p + 4);
	year = str2int(tok.p + 4, tok.p + 6) + 2000;

	if ((day | mon | year) < 0) {
		D("date not properly formatted: '%.*s'", tok.end - tok.p, tok.p);
		return -1;
	}

	r->utc_year  = year;
	r->utc_mon   = mon;
	r->utc_day   = day;
	return nmea_reader_update_time(r, mtime);
}


static double
convert_from_hhmm(Token  tok)
{
	double  val     = str2float(tok.p, tok.end);
	int     degrees = (int)(floor(val) / 100);
	double  minutes = val - degrees * 100.;
	double  dcoord  = degrees + minutes / 60.0;
	return dcoord;
}


static int
nmea_reader_update_latlong(NmeaReader*  r,
                           Token        latitude,
                           char         latitudeHemi,
                           Token        longitude,
                           char         longitudeHemi)
{
	double   lat, lon;
	Token    tok;
	tok = latitude;

	if (tok.p + 6 > tok.end) {
		D("latitude is too short: '%.*s'", tok.end - tok.p, tok.p);
		return -1;
	}

	lat = convert_from_hhmm(tok);

	if (latitudeHemi == 'S')
		lat = -lat;

	tok = longitude;

	if (tok.p + 6 > tok.end) {
		D("longitude is too short: '%.*s'", tok.end - tok.p, tok.p);
		return -1;
	}

	lon = convert_from_hhmm(tok);

	if (longitudeHemi == 'W')
		lon = -lon;

	r->fix.flags    |= GPS_LOCATION_HAS_LAT_LONG;
	r->fix.latitude  = lat;
	r->fix.longitude = lon;
	return 0;
}


static int
nmea_reader_update_altitude(NmeaReader*  r,
                            Token        altitude,
                            Token        units)
{
	double  alt;
	Token   tok = altitude;

	if (tok.p >= tok.end)
		return -1;

	r->fix.flags   |= GPS_LOCATION_HAS_ALTITUDE;
	r->fix.altitude = str2float(tok.p, tok.end);
	return 0;
}

static int
nmea_reader_update_accuracy(NmeaReader*  r,
                            Token        accuracy)
{
	double  acc;
	Token   tok = accuracy;

	if (tok.p >= tok.end)
		return -1;

	//tok is cep*cc, we only want cep
	r->fix.accuracy = str2float(tok.p, tok.end);

	if (r->fix.accuracy == 99.99) {
		return 0;
	}

	r->fix.flags   |= GPS_LOCATION_HAS_ACCURACY;
	return 0;
}

static int
nmea_reader_update_bearing(NmeaReader*  r,
                           Token        bearing)
{
	double  alt;
	Token   tok = bearing;

	if (tok.p >= tok.end)
		return -1;

	r->fix.flags   |= GPS_LOCATION_HAS_BEARING;
	r->fix.bearing  = str2float(tok.p, tok.end);
	return 0;
}


static int
nmea_reader_update_speed(NmeaReader*  r,
                         Token        speed)
{
	double  alt;
	Token   tok = speed;

	if (tok.p >= tok.end)
		return -1;

	r->fix.flags   |= GPS_LOCATION_HAS_SPEED;
	r->fix.speed = str2float(tok.p, tok.end) / 1.852;
	return 0;
}

static int
nmea_reader_update_timemap(NmeaReader* r,
                           Token       systime_tok,
                           Token       timestamp_tok)
{
	int ret;
	time_t timestamp;

	if (systime_tok.p >= systime_tok.end ||
	        timestamp_tok.p >= timestamp_tok.end) {
		r->timemap.valid = 0;
		return -1;
	}

	ret = nmea_reader_get_timestamp(r, timestamp_tok, &timestamp);

	if (ret) {
		r->timemap.valid = 0;
		return ret;
	}

	r->timemap.valid = 1;
	r->timemap.systime = str2float(systime_tok.p, systime_tok.end);
	r->timemap.timestamp = (GpsUtcTime)((long long)timestamp * 1000);
	return 0;
}


static void
nmea_reader_parse(NmeaReader*  r)
{
	/* we received a complete sentence, now parse it to generate
	 * a new GPS fix...
	 */
	NmeaTokenizer  tzer[1];
	Token          tok;

	// D("Received: '%.*s'", r->pos, r->in);

	if (r->pos < 9) {
		D("Too short. discarded.");
		return;
	}

	if (g_gps_state->callbacks.nmea_cb) {
		struct timeval tv;
		unsigned long long mytimems;
		gettimeofday(&tv, NULL);
		mytimems = tv.tv_sec * 1000 + tv.tv_usec / 1000;
		g_gps_state->callbacks.nmea_cb(mytimems, r->in, r->pos);
	}

	nmea_tokenizer_init(tzer, r->in, r->in + r->pos);
#ifdef GPS_DEBUG_TOKEN
	{
		int  n;
		D("Found %d tokens", tzer->count);

		for (n = 0; n < tzer->count; n++) {
			Token  tok = nmea_tokenizer_get(tzer, n);
			D("%2d: '%.*s'", n, tok.end - tok.p, tok.p);
		}
	}
#endif
	tok = nmea_tokenizer_get(tzer, 0);
	// ignore first two characters.
	tok.p += 2;

	if (!memcmp(tok.p, "GGA", 3)) {
		// GPS fix
		Token  tok_fixstaus      = nmea_tokenizer_get(tzer, 6);

		if (tok_fixstaus.p[0] > '0') {
			Token  tok_time          = nmea_tokenizer_get(tzer, 1);
			Token  tok_latitude      = nmea_tokenizer_get(tzer, 2);
			Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer, 3);
			Token  tok_longitude     = nmea_tokenizer_get(tzer, 4);
			Token  tok_longitudeHemi = nmea_tokenizer_get(tzer, 5);
			Token  tok_altitude      = nmea_tokenizer_get(tzer, 9);
			Token  tok_altitudeUnits = nmea_tokenizer_get(tzer, 10);
			nmea_reader_update_time(r, tok_time);
			nmea_reader_update_latlong(r, tok_latitude,
			                           tok_latitudeHemi.p[0],
			                           tok_longitude,
			                           tok_longitudeHemi.p[0]);
			nmea_reader_update_altitude(r, tok_altitude, tok_altitudeUnits);
		}
	} else if (!memcmp(tok.p, "GLL", 3)) {
		Token  tok_fixstaus      = nmea_tokenizer_get(tzer, 6);

		if (tok_fixstaus.p[0] == 'A') {
			Token  tok_latitude      = nmea_tokenizer_get(tzer, 1);
			Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer, 2);
			Token  tok_longitude     = nmea_tokenizer_get(tzer, 3);
			Token  tok_longitudeHemi = nmea_tokenizer_get(tzer, 4);
			Token  tok_time          = nmea_tokenizer_get(tzer, 5);
			nmea_reader_update_time(r, tok_time);
			nmea_reader_update_latlong(r, tok_latitude,
			                           tok_latitudeHemi.p[0],
			                           tok_longitude,
			                           tok_longitudeHemi.p[0]);
		}
	} else if (!memcmp(tok.p, "GSA", 3)) {
		Token  tok_fixStatus   = nmea_tokenizer_get(tzer, 2);
		int i;

		if (tok_fixStatus.p[0] != '\0' && tok_fixStatus.p[0] != '1') {
			g_gps_state->fixed = 1;
			r->sv_status.used_in_fix_mask = 0ul;

			for (i = 3; i <= 14; ++i) {
				Token  tok_prn  = nmea_tokenizer_get(tzer, i);
				int prn = str2int(tok_prn.p, tok_prn.end);

				/* only available for PRN 1-32 */
				if ((prn > 0) && (prn < 33)) {
					r->sv_status.used_in_fix_mask |= (1ul << (prn - 1));
					r->sv_status_changed = 1;
					/* mark this parameter to identify the GSA is in fixed state */
					r->gsa_fixed = 1;
				}
			}
		} else {
			g_gps_state->fixed = 0;

			if (r->gsa_fixed == 1) {
				D("%s: GPGSA fixed -> unfixed", __FUNCTION__);
				r->sv_status.used_in_fix_mask = 0ul;
				r->sv_status_changed = 1;
				r->gsa_fixed = 0;
			}
		}
	} else if (!memcmp(tok.p, "GSV", 3)) {
		Token  tok_noSatellites  = nmea_tokenizer_get(tzer, 3);
		int    noSatellites = str2int(tok_noSatellites.p, tok_noSatellites.end);

		if (noSatellites > 0) {
			Token  tok_noSentences   = nmea_tokenizer_get(tzer, 1);
			Token  tok_sentence      = nmea_tokenizer_get(tzer, 2);
			int sentence = str2int(tok_sentence.p, tok_sentence.end);
			int totalSentences = str2int(tok_noSentences.p, tok_noSentences.end);
			int curr;
			int i;

			if (sentence == 1) {
				r->sv_status_changed = 0;
				r->sv_status.num_svs = 0;
			}

			curr = r->sv_status.num_svs;
			i = 0;

			while (i < 4 && r->sv_status.num_svs < noSatellites) {
				Token  tok_prn = nmea_tokenizer_get(tzer, i * 4 + 4);
				Token  tok_elevation = nmea_tokenizer_get(tzer, i * 4 + 5);
				Token  tok_azimuth = nmea_tokenizer_get(tzer, i * 4 + 6);
				Token  tok_snr = nmea_tokenizer_get(tzer, i * 4 + 7);
				r->sv_status.sv_list[curr].prn = str2int(tok_prn.p, tok_prn.end);
				r->sv_status.sv_list[curr].elevation = str2float(tok_elevation.p, tok_elevation.end);
				r->sv_status.sv_list[curr].azimuth = str2float(tok_azimuth.p, tok_azimuth.end);

				if (str2float(tok_snr.p, tok_snr.end) == 99) {
					r->sv_status.sv_list[curr].snr = 0;
				} else {
					r->sv_status.sv_list[curr].snr = str2float(tok_snr.p, tok_snr.end);
				}

				r->sv_status.num_svs += 1;
				curr += 1;
				i += 1;
			}

			if (sentence == totalSentences) {
				r->sv_status_changed = 1;
			}
		}
	} else if (!memcmp(tok.p, "RMC", 3)) {
		Token  tok_fixStatus     = nmea_tokenizer_get(tzer, 2);

		if (tok_fixStatus.p[0] == 'A') {
			Token  tok_time          = nmea_tokenizer_get(tzer, 1);
			Token  tok_latitude      = nmea_tokenizer_get(tzer, 3);
			Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer, 4);
			Token  tok_longitude     = nmea_tokenizer_get(tzer, 5);
			Token  tok_longitudeHemi = nmea_tokenizer_get(tzer, 6);
			Token  tok_speed         = nmea_tokenizer_get(tzer, 7);
			Token  tok_bearing       = nmea_tokenizer_get(tzer, 8);
			Token  tok_date          = nmea_tokenizer_get(tzer, 9);
			nmea_reader_update_date(r, tok_date, tok_time);
			nmea_reader_update_latlong(r, tok_latitude,
			                           tok_latitudeHemi.p[0],
			                           tok_longitude,
			                           tok_longitudeHemi.p[0]);
			nmea_reader_update_bearing(r, tok_bearing);
			nmea_reader_update_speed(r, tok_speed);
		}
	} else if (!memcmp(tok.p, "VTG", 3)) {
		Token  tok_fixStatus     = nmea_tokenizer_get(tzer, 9);

		if (tok_fixStatus.p[0] != '\0' && tok_fixStatus.p[0] != 'N') {
			Token  tok_bearing       = nmea_tokenizer_get(tzer, 1);
			Token  tok_speed         = nmea_tokenizer_get(tzer, 5);
			nmea_reader_update_bearing(r, tok_bearing);
			nmea_reader_update_speed(r, tok_speed);
		}
	} else {
		tok.p -= 2;
		D("unknown sentence '%.*s", tok.end - tok.p, tok.p);
	}

	g_gps_state->callbacks.sv_status_cb(&r->sv_status);

	if (g_gps_state->fixed && g_gps_state->init &&
	        r->fix.flags & GPS_LOCATION_HAS_LAT_LONG) {
		if (g_gps_state->callbacks.location_cb) {
			g_gps_state->callbacks.location_cb(&r->fix);
			r->fix.flags = 0;
		}
	}
}

static void
nmea_reader_addc(NmeaReader*  r, int  c)
{
	int cnt;

	if (r->overflow) {
		r->overflow = (c != '\n');
		return;
	}

	if (r->pos >= (int) sizeof(r->in) - 1) {
		r->overflow = 1;
		r->pos      = 0;
		return;
	}

	r->in[r->pos] = (char)c;
	r->pos       += 1;

	if (c == '\n') {
		// D("Parsing NMEA commands");
		nmea_reader_parse(r);
		r->pos = 0;
	}
}

/*****************************************************************/
/*****************************************************************/
/*****                                                       *****/
/*****       C O N N E C T I O N   S T A T E                 *****/
/*****                                                       *****/
/*****************************************************************/
/*****************************************************************/


static void* gps_task_thread(void*  arg)
{
	GpsState*   state = (GpsState*) arg;
	NmeaReader	*reader;
	int         epoll_fd   = epoll_create(1);
	int         gps_fd     = state->fd;
	// register control file descriptors for polling
	epoll_register(epoll_fd, gps_fd);
	reader = &state->reader;
	nmea_reader_init(reader);
	D("gps task thread running");
	char buf[512];
	int  nn, ret;
	D("gps fd event start");

	for (;;) {
		struct epoll_event   events[1];
		int                  ne, nevents;
		nevents = epoll_wait(epoll_fd, events, 1, -1);

		if (nevents < 0) {
			if (errno != EINTR) {
				ALOGD("epoll_wait() unexpected error: %s", strerror(errno));
			}

			continue;
		}

		for (ne = 0; ne < nevents; ne++) {
			if ((events[ne].events & (EPOLLERR | EPOLLHUP)) != 0) {
				ALOGD("EPOLLERR or EPOLLHUP after epoll_wait() !?");
				goto Exit;
			}

			if ((events[ne].events & EPOLLIN) != 0) {
				do {
					fd_set readfds;
					FD_ZERO(&readfds);
					FD_SET(gps_fd, &readfds);
					ret = select(FD_SETSIZE, &readfds, NULL, NULL, NULL);

					if (ret < 0)
						continue;

					if (FD_ISSET(gps_fd, &readfds))
						ret = read(gps_fd, buf, sizeof(buf));

					//D("\n---bufStart--%s--bufEnd-------\n", buf);
				} while (ret < 0 && errno == EINTR);

				if (ret > 0)
					for (nn = 0; nn < ret; nn++)
						nmea_reader_addc(reader, buf[nn]);
			}
		}
	}

	/* end MSTAR in/out sentences */
	// D("gps fd event end");
Exit:
	
	return NULL;
}


static void* gps_state_thread(void*  arg)
{
	GpsState*   state = (GpsState*) arg;
	int         epoll_fd   = epoll_create(1);
	int         started    = 0;
	//int         gps_fd     = state->fd;
	int         control_fd = state->control[1];
	//update_utc_diff();
	// register control file descriptors for polling
	epoll_register(epoll_fd, control_fd);
	//epoll_register(epoll_fd, gps_fd);
	D("gps thread running");

	// now loop
	for (;;) {
		struct epoll_event   events[1];
		int                  ne, nevents;
		nevents = epoll_wait(epoll_fd, events, 1, -1);

		if (nevents < 0) {
			if (errno != EINTR) {
				ALOGD("epoll_wait() unexpected error: %s", strerror(errno));
			}

			continue;
		}

		//D("gps thread received %d events", nevents);
		for (ne = 0; ne < nevents; ne++) {
			if ((events[ne].events & (EPOLLERR | EPOLLHUP)) != 0) {
				ALOGD("EPOLLERR or EPOLLHUP after epoll_wait() !?");
				goto Exit;
			}

			if ((events[ne].events & EPOLLIN) != 0) {
				int  fd = events[ne].data.fd;

				if (fd == control_fd) {
					char  cmd = 255;
					int   ret;
					D("gps control fd event");

					do {
						ret = read(fd, &cmd, 1);
					} while (ret < 0 && errno == EINTR);

					//
					if (cmd == CMD_QUIT) {
						D("gps thread quitting on demand");
						goto Exit;
					} else if (cmd == CMD_START) {
						if (!started) {
							//D("gps thread starting  location_cb=%p", state->callbacks.location_cb);
							started = 1;
							g_status.status = GPS_STATUS_SESSION_BEGIN ;
							(state->callbacks).status_cb(&g_status);
						}
					} else if (cmd == CMD_STOP) {
						//
						if (started) {
							D("gps thread stopping");
							started = 0;
							g_status.status = GPS_STATUS_SESSION_END;
							(state->callbacks).status_cb(&g_status);
						}
					}
				} else {
					ALOGD("epoll_wait() returned unkown fd %d ?", fd);
				}
			}
		}
	}

Exit:
	return NULL;
}


static int gps_state_init(GpsState*  state)
{
	D("--- gps_state_init ---\n");
	GpsCallbacks* callbacks = &(state->callbacks);
	state->fd = open_gps_fd();

	if (state->fd < 0) {
		ALOGD("could not open gps serial device %s: %s", MSTAR_DEVICES, strerror(errno));
		goto Fail;
	} else {
		D(" open_gps_fd state->fd: %d",  state->fd);
	}

	if (socketpair(AF_LOCAL, SOCK_STREAM, 0, state->control) < 0) {
		ALOGD("could not create thread control socket pair: %s", strerror(errno));
		goto FailSocket;
	}

	if ((g_gps_state->thread = callbacks->create_thread_cb("mstar_gps",
	                           gps_state_thread, (void *)g_gps_state)) < 0) {
		ALOGD("could not create gps thread gps_state_thread: %s ", strerror(errno));
	}

	D("gps state initialized ");
	return 0;
FailSocket:
	close(state->fd);
	state->fd = -1;
Fail:
	return -1;
}

static int mstar_gps_init(GpsCallbacks* callbacks)
{
	D("rpdzkj_gps_init\n");
	
#if 0  
	int fd1,fd2,fd3;
	char cam_file1[] = "/proc/rp_button/button_ctrl";	
	char cam_file2[] = "proc/rpgps";	
	char cam_file3[] = "dev/rp_bt_dev";
//	char cam_file4[] = "sys/class/rp_bt_dev";
	
	 ALOGD(" ");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD("                          rongpin gps");
	 ALOGD("                          www.rpdzkj.com");
	 ALOGD("                       TEL: 0755 - 23574363");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD(" ");

	fd1 = open(cam_file1,O_RDWR);	
	if(fd1 > 0 ){						
		close(fd1);	
	}	
	else{		
			
			close(fd1);	
			return -1; 
	}


	 fd2 = open(cam_file2,O_RDWR);
	if(fd2 > 0){
		close(fd2);
	}
	else{
		close(fd2);
		return -1;
	}
	
	fd3 = open(cam_file3,O_RDWR);
	if(fd3 > 0){
		close(fd3);
	}
	else{
		close(fd3);
		return -1;
	}

#endif
	g_gps_state->init = 0;
	g_gps_state->start  = 0;
	g_gps_state->control[0] = -1;
	g_gps_state->control[1] = -1;
	g_gps_state->fd         = -1;
	//g_gps_state->freq        = 1000;     //1000 ms
	g_gps_state->callbacks   = *callbacks;
	g_gps_state->init = 1;
	gps_state_init(g_gps_state);
	g_status.status = GPS_STATUS_ENGINE_ON;
	(g_gps_state->callbacks).status_cb(&g_status);;
	return 0;
}

static int mstar_gps_start()
{
	gps_hardware_poweron();
	GpsCallbacks* callbacks = &(g_gps_state->callbacks);

	if ((g_gps_state->task_thread = callbacks->create_thread_cb("mstar_gps",
	                                gps_task_thread, (void *)g_gps_state)) < 0) {
		ALOGD("could not create gps thread gps_task_thread: %s", strerror(errno));
		goto FailThread;
	}

	if (internal_req(CMD_START)) {
		return -1;
	}

	g_gps_state->start = 1;
	return 0;
FailThread:
	close(g_gps_state->control[0]);
	g_gps_state->control[0] = -1;
	close(g_gps_state->control[1]);
	g_gps_state->control[1] = -1;
	return -1;
}

static int mstar_gps_stop()
{
	GpsState*  s = g_gps_state;
	void*  dummy;
	D("mstar_gps_stop\n");
	//(g_gps_state->callbacks).sv_status_cb = NULL;
	//(g_gps_state->callbacks).location_cb = NULL;
	pthread_join(s->task_thread, &dummy);
	//gps_hardware_poweroff();

	if (internal_req(CMD_STOP)) {
		return -1;
	}

	//gps_state_done(g_gps_state);
	g_gps_state->start = 0;
	return 0;
}

static void mstar_gps_cleanup(void)
{
	D("----------rpdzkj_gps_cleanup--------");
	GpsState*  s = g_gps_state;
	gps_state_done(s);
	//(g_gps_state->callbacks).location_cb = NULL;
	//(g_gps_state->callbacks).status_cb = NULL;
	//(g_gps_state->callbacks).sv_status_cb = NULL;
	g_gps_state->init   = 0;
	D("----------mstar_gps_cleanup---end-----");
	return;
}

static void
mstar_gps_delete_aiding_data(GpsAidingData flags)
{
	if (internal_req(CMD_CLEAR_AID)) {
		return;
	}
	return;
}

static int mstar_gps_inject_location(double latitude, double longitude, float accuracy)
{
	/* not yet implemented */
	return 0;
}

static int
mstar_gps_inject_time(GpsUtcTime time, int64_t timeReference, int uncertainty)
{
	//uncertainty       ntp transmitTime
	//timeReference     when we got the ntp times.
	//time              ntp time
	//gnx call GN_GPS_Read_UTC to get the rtc times. so this func is unuseful.
	return 0;
}

static int mstar_gps_set_position_mode(GpsPositionMode mode, int fix_frequency)
{
	//the jni call this func int android_location_GpsLocationProvider_start
	//          //android_location_GpsLocationProvider_start()
	//    int result = sGpsInterface->set_position_mode(GPS_POSITION_MODE_STANDALONE, (singleFix ? 0 : fixFrequency));
	//    if (result) {
	//          return result;
	//    }
	//    return (sGpsInterface->start() == 0);
	//FIXME: in future, we must implement this func, the ASSISTED maybe usefull for performances.
	return 0;
}

static const void* mstar_gps_get_extension(const char* name)
{
	return NULL;
}


static const GpsInterface  mstarGpsInterface = {
	sizeof(GpsInterface),
	mstar_gps_init,
	mstar_gps_start,
	mstar_gps_stop,
	mstar_gps_cleanup,
	mstar_gps_inject_time,
	mstar_gps_inject_location,
	mstar_gps_delete_aiding_data,
	mstar_gps_set_position_mode,
	mstar_gps_get_extension,
};

const GpsInterface* gps_get_hardware_interface()
{
	return &mstarGpsInterface;
}

const GpsInterface* gps_get_interface()
{
	return gps_get_hardware_interface();
}

static int open_gps(const struct hw_module_t* module, char const* name,
                    struct hw_device_t** device)
{
	struct gps_device_t *dev = malloc(sizeof(struct gps_device_t));
	
	
#if 0  
	int fd1,fd2,fd3;
	char cam_file1[] = "/proc/rp_button/button_ctrl";	
	char cam_file2[] = "proc/rpgps";	
	char cam_file3[] = "dev/rp_bt_dev";
//	char cam_file4[] = "sys/class/rp_bt_dev";
	
	 ALOGD(" ");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD("                          rongpin gps");
	 ALOGD("                          www.rpdzkj.com");
	 ALOGD("                       TEL: 0755 - 23574363");
	 ALOGD("-------------------------------------------------------------");
	 ALOGD(" ");

	fd1 = open(cam_file1,O_RDWR);	
	if(fd1 > 0 ){						
		close(fd1);	
	}	
	else{		
			
			close(fd1);	
			return -1; 
	}


	 fd2 = open(cam_file2,O_RDWR);
	if(fd2 > 0){
		close(fd2);
	}
	else{
		close(fd2);
		return -1;
	}
	
	fd3 = open(cam_file3,O_RDWR);
	if(fd3 > 0){
		close(fd3);
	}
	else{
		close(fd3);
		return -1;
	}

#endif

	memset(dev, 0, sizeof(*dev));
	dev->common.tag = HARDWARE_DEVICE_TAG;
	dev->common.version = 0;
	dev->common.module = (struct hw_module_t*)module;
	dev->get_gps_interface = gps_get_interface;
	*device = (struct hw_device_t*)dev;
	return 0;
}

static struct hw_module_methods_t gps_module_methods = {
	.open = open_gps
};

//const
struct hw_module_t HAL_MODULE_INFO_SYM = {
	.tag = HARDWARE_MODULE_TAG,
	.version_major = 1,
	.version_minor = 0,
	.id = GPS_HARDWARE_MODULE_ID,
	.name = "MStar GPS Module",
	.author = "Andy",
	.methods = &gps_module_methods,
};