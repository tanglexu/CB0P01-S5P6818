LOCAL_PATH := $(my-dir)

ifneq ($(strip $(OEM_Name)),)
OEM_PATH ?= $(OEM_Name).$(Product_ID)

ifeq ($(strip $(OEM_PATH)),CB0P01-S5P6818.POS)

include $(CLEAR_VARS)
LOCAL_MODULE := bootanimation.zip
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := bootanimation.zip
LOCAL_MODULE_CLASS := ETC
LOCAL_MODULE_SUFFIX := 
LOCAL_MODULE_PATH := $(TARGET_OUT)/media
include $(BUILD_PREBUILT)

endif

endif
