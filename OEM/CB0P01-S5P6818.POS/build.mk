
# set TCP port for adb debugging
PRODUCT_PROPERTY_OVERRIDES += \
    service.adb.tcp.port=5555

# set default Time Zone
PRODUCT_PROPERTY_OVERRIDES += \
	persist.sys.timezone=Asia/Taipei
        
# set default country/language
PRODUCT_PROPERTY_OVERRIDES += \
    persist.sys.language=zh \
	persist.sys.country=TW

# boot animation
ifeq ($(HAS_VIDEO_LOGO),Y)
PRODUCT_PACKAGES += \
    bootanimation.zip
endif

# prebuilt APKs
PRODUCT_PACKAGES += \
	File_manager \
	com.googlecode.eyesfree.setorientation

#	eGalaxAutoCalib \

