type		=1      //RGB:0 LVDS:1 MIPI:2

out_format	=3	//CFG_DISP_PRI_OUT_FORMAT
//RGB555:0  RGB565:1  RGB666:2  RGB888:3  MRGB555A:4  MRGB555B:5  MRGB565:6  MRGB666:7  MRGB888A:8  MRGB888B:9  CCIR656:10  CCIR601A:12  CCIR601B:13


lvds_format	=1		//LVDS_LCDFORMAT_VESA:0   LVDS_LCDFORMAT_JEIDA:1
pclk		=67000000	//CFG_DISP_PRI_PIXEL_CLOCK
width		=1280		//CFG_DISP_PRI_RESOL_WIDTH
height		=800		//CFG_DISP_PRI_RESOL_HEIGHT
hfp		=160		//CFG_DISP_PRI_HSYNC_FRONT_PORCH
hbp		=160		//CFG_DISP_PRI_HSYNC_BACK_PORCH
hls		=20		//CFG_DISP_PRI_HSYNC_SYNC_WIDTH
vfp		=12		//CFG_DISP_PRI_VSYNC_FRONT_PORCH
vbp		=23		//CFG_DISP_PRI_VSYNC_BACK_PORCH
vls		=3		//CFG_DISP_PRI_VSYNC_SYNC_WIDTH

gpio_led	=pad_gpio_c + 24 : 1 : 0	//goio num（pad_gpio_c + 24） : alt（1） :value（0）
gpio1		=pad_gpio_c + 7 : 1 : 1		//gpio 可以如下自行增加，此处修改只提供作为gpio使用，，注意使ALT选择为GPIO   如J60的4个gpio
gpio2		=pad_gpio_c + 27 : 0 : 1
gpio3		=pad_gpio_b + 30 : 1 : 1
gpio4		=pad_gpio_b + 31 : 1 : 1